#!/bin/zsh

# ******************** envs ********************

export ZSH_BASE_DIR=$(dirname $(readlink -f "$0"))
export ZSH_PLUG_DIR="$ZSH_BASE_DIR/plug"

# ******************** zinit ********************
if [ -f "$ZSH_BASE_DIR/zinit.sh" ]; then
    source $ZSH_BASE_DIR/zinit.sh
fi

# ******************** plug ********************

if [ -d "$ZSH_BASE_DIR/plug" ]; then
    if find "$ZSH_BASE_DIR/plug" -type f -name "*.sh" -print -quit | grep -q .; then
        local sh_files=("$ZSH_BASE_DIR/plug"/*.sh)
        for sh_file in "${sh_files[@]}"; do
            source $sh_file
        done
    fi
fi
