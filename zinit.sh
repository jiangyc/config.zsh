
## Load zinit
export ZINIT_HOME="$HOME/.local/share/zinit/zinit.git"
if [ ! -f "$ZINIT_HOME/zinit.zsh" ]; then
    return 0
fi
source "${ZINIT_HOME}/zinit.zsh"

## oh-my-zsh plugins
# lib
zinit snippet OMZL::git.zsh
zinit snippet OMZL::clipboard.zsh
zinit snippet OMZL::history.zsh
zinit snippet OMZL::completion.zsh
# zinit snippet OMZL::termsupport.zsh

# plugins
zinit snippet OMZP::git
zinit snippet OMZP::sudo
zinit snippet OMZP::extract

## others plugins
zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-autosuggestions

## themes
# oh-my-zsh themes
setopt promptsubst
zinit snippet OMZT::agnoster
# zinit snippet OMZT::robbyrussell

# powerline10k
# zinit ice depth"1"
# zinit light romkatv/powerlevel10k
