#!/bin/bash

# ******************** Envs ********************

[ -z "$HTTP_PROXY_HOST" ] && export HTTP_PROXY_HOST="127.0.0.1"
[ -z "$HTTP_PROXY_PORT" ] && export HTTP_PROXY_PORT="11223"

http_proxy__help() {
	echo "http_proxy: Command line proxy switching tool."
	echo "usage: http_proxy <option> [args]"
	echo
	echo "  options:"
	echo "    -h/--help                   - Show this help documentation"
	echo "    -s/--status                 - Display the status of each configured proxy"
	echo "    -e/--enable  [term/git/all] - Enable proxy for term/git, default to term"
	echo "    -d/--disable [term/git/all] - Disable proxy for term/git, default to term"
	echo
}

http_proxy__status() {
	if [[ -z "$1" || "$1" == "term" || "$1" == "all" ]]; then
		echo "term:"
		echo "  http_proxy:  $http_proxy"
		echo "  https_proxy: $https_proxy"
	fi

	if [[ "$1" == "git" || "$1" == "all" ]]; then
		echo "git:"
		echo "  http.proxy:  $(git config --global http.proxy)"
		echo "  https.proxy: $(git config --global https.proxy)"
	fi
}

http_proxy__enable() {
	local http_proxy_url="http://$HTTP_PROXY_HOST:$HTTP_PROXY_PORT"

	if [[ -z "$1" || "$1" == "term" || "$1" == "all" ]]; then
		export http_proxy="$http_proxy_url"
		export https_proxy="$http_proxy_url"
	fi

	if [[ "$1" == "git" || "$1" == "all" ]]; then
		git config --global http.proxy $http_proxy_url
		git config --global https.proxy $http_proxy_url
	fi
}

http_proxy__disable() {
	if [[ -z "$1" || "$1" == "term" || "$1" == "all" ]]; then
		if [ ! -z "$http_proxy" ]; then
			unset http_proxy
		fi
		if [ ! -z "$https_proxy" ]; then
			unset https_proxy
		fi
	fi

	if [[ "$1" == "git" || "$1" == "all" ]]; then
		if [ ! -z "$(git config --global http.proxy)" ]; then
			git config --global --unset http.proxy
		fi
		if [ ! -z "$(git config --global https.proxy)" ]; then
			git config --global --unset https.proxy
		fi
	fi
}

http_proxy() {
	if [[ -z "$1" || "$1" == "-h" || "$1" == "--help" ]]; then
		http_proxy__help
	elif [[ "$1" == "-s" || "$1" == "--status" ]]; then
		http_proxy__status "$2"
	elif [[ "$1" == "-e" || "$1" == "--enable" ]]; then
		http_proxy__enable "$2"
		http_proxy__status "$2"
	elif [[ "$1" == "-d" || "$1" == "--disable" ]]; then
		http_proxy__disable "$2"
		http_proxy__status "$2"
	else
		echo "invalid option: $1"
		http_proxy__help
	fi
}
