alias vi="vim"

if [ -f "/usr/bin/exa" ]; then
	alias ll="exa -lh"
else
	alias ll="ls --color -lh"
fi
alias ls="ls --color"
