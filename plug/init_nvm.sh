#!/bin/bash

export NVM_DIR=$HOME/.local/share/nvm

if [ -f "/usr/share/nvim/init-nvm.sh" ]; then
    if [ ! -f "/usr/bin/vfox" ]; then
        source /usr/share/nvim/init-nvm.sh
    fi
fi
