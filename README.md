# config.zsh

my zsh profile.

## 安装

* zinit plugin manager

```bash
git clone https://github.com/zdharma-continuum/zinit.git ~/.local/share/zinit/zinit.git
# update
# zinit self-update
```

* my profile

```bash
git clone https://gitlab.com/jiangyc/config.zsh ~/.local/etc/profile.d
echo 'source "~~/.local/etc/profile.d/init.sh"' >> ~/.zshrc
```
